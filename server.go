/**
* Created by Visual Studio Code.
* User: tuxer
* Created At: 2018-07-12 18:37:20
**/

package apiserver

import (
	"io/ioutil"
	"strings"
	"time"

	"gitlab.com/tuxer/go-db"
	"gitlab.com/tuxer/go-json"
)

//Server ...
type Server struct {
	cn   *db.Connection
	path string

	config     json.Object
	configFile string
	configTime time.Time
}

//Execute ...
func (s *Server) Execute(command string, data json.Object) (res json.Object) {
	res = make(json.Object)

	defer func() {
		if res != nil {
			res.Put(`command`, command).Put(`status`, 0)
			if r := recover(); r != nil {
				res.Put(`message`, `unknown error`).Put(`status`, 99)
				switch r := r.(type) {
				case string:
					res.Put(`message`, r)
				case error:
					res.Put(`message`, r.Error())
				}
			}
		}
	}()

	if cfg := s.config.GetJSONObject(command); cfg != nil {
		dbParams := []interface{}{}

		if params := cfg.GetString(`params`); params != `` {
			splitParams := strings.Split(params, `,`)
			for _, val := range splitParams {
				dbParams = append(dbParams, getRequiredParams(data, strings.TrimSpace(val)))
			}
		}
		query := cfg.GetString(`query`)
		switch typeName := cfg.GetStringOr(`type`, `select`); typeName {
		case `select`:
			if rs, e := s.cn.Select(query, dbParams...); e != nil {
				res.Put(`message`, e.Error())
			} else {
				res.Put(`data`, rs)
			}
		case `get`:
			if rs, e := s.cn.Get(query, dbParams...); e != nil {
				res.Put(`message`, e.Error())
			} else {
				res.Put(`data`, rs)
			}
		case `insert`:
			if rs, e := s.cn.Exec(query, dbParams...); e != nil {
				res.Put(`message`, e.Error())
			} else {
				if id, e := rs.LastInsertID(); e == nil {
					res.Put(`data`, id)
				}
			}
		case `update`:
			fallthrough
		case `delete`:
			if rs, e := s.cn.Exec(query, dbParams...); e != nil {
				res.Put(`message`, e.Error())
			} else {
				if row, e := rs.RowsAffected(); e == nil {
					res.Put(`data`, row)
				}
			}
		default:
			res.Put(`message`, `invalid config, `+typeName+` not supported`)
		}
	} else {
		res = nil
	}
	return res
}

func getRequiredParams(data json.Object, name string) string {
	var val *string
	if val = data.GetStringNull(name); val == nil {
		val = data.GetStringNull(`data.` + name)
	}
	if val == nil {
		panic(`missing required param: ` + name)
	}
	return *val
}

//LoadConfig ...
func (s *Server) LoadConfig(configFile string) bool {
	s.configFile = configFile
	if data, e := ioutil.ReadFile(configFile); e == nil {
		s.config = json.Parse(data)
		return true
	}
	return false
}

//SetConnection ...
func (s *Server) SetConnection(cn *db.Connection) {
	s.cn = cn
}

//New ...
func New(cn *db.Connection, path, configFile string) *Server {
	if !strings.HasPrefix(path, `/`) {
		path = `/` + path
	}
	svr := &Server{path: path}
	svr.SetConnection(cn)
	svr.LoadConfig(configFile)
	return svr
}
